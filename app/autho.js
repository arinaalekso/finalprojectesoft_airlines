import React from "react"
import {observer} from "mobx-react"
import SessionArchive from "./frontend/airlines/src/archive/session_archive";
import './frontend/airlines/src/autho.css'
import ButtonReg from "./frontend/airlines/src/components/buttons_reg";

function Auth() 
{
    return (
        <div className="open_autho"> 
            <H1>Войдите в АИС «АВИАПРЕДПРИТИЕ»</H1>  
            <form>
                <div className="open_forma">
                    <p>
                        <input type="text" name="login" placeholder="Логин"/>
                    </p>  
                    <p className="com1"></p>
                    <p>
                        <input type="password" name="password" placeholder="Пароль" />
                    </p>
                    <p className="com2"></p>
                    <input type="submit" value="Вход" onClick={SessionArchive.processEntry}  name="log_in" />  
                    {console.log('SessionArchive.processEntry')}
                    <script src="./frontend/src/components/validation_data.js"></script>
                    <button className="btn_reg">РЕГИСТРАЦИЯ</button>
                    <ButtonReg/>
                </div>                
            </form>
        </div>
    );
}

export default observer(Auth);