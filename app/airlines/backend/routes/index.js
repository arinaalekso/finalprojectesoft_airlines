const Router =require('express')
const router = new Router()
const personalaccRouter = require('./personalaccRouter')
const authoRouter=require('./authoRouter')
const AircraftControllers = require('../controllers/aircraftControllers')
const EngTechControllers =require('../controllers/engineertechnicalControllers')
const FlightPersonalControllers = require('../controllers/flightpersonalControllers')
const FlightControllers = require('../controllers/flightsControllers')
const MRControllers = require('../controllers/maintenancereportControllers')

router.use('/personalacc', personalaccRouter)
router.use('/autho',authoRouter)
router.get('/flight', FlightControllers.getShow)
router.get('/flightpersonal', FlightPersonalControllers.getShow)
router.get('/eng_tech',EngTechControllers.getShow);
router.post('/aircraft')
router.get('/aircraft', AircraftControllers.getShow);
router.get('/mr', MRControllers.getShow)

module.exports=router