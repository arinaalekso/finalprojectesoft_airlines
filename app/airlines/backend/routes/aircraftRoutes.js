const Router =require('express')
const router = new Router()
const AircraftControllers = require('../controllers/aircraftControllers')

router.post('/aircraft')
router.get('/aircraft', AircraftControllers.getShow);

module.exports=router