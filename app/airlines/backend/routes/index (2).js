const Router =require('express')
const router = new Router()
const aircraftRouter = require('./aircraftRouter')
const flightsRouter = require('./flightsRouter')
const flightpersonalRouter = require('./flightpersonalRouter')
const engineertechnicalRouter = require('./engineertechnicalRouter')
const mrRouter = require('./maintenancereportRouter')
const personalaccRouter = require('./personalaccRouter')
const authoRouter=require('./authoRouter')

router.use('/personalacc', personalaccRouter)
router.use('/autho',authoRouter)
router.use('/flight', flightsRouter)
router.use('/flightpersonal', flightpersonalRouter)
router.use('/engineer_technical',engineertechnicalRouter)
router.use('/aircraft',aircraftRouter)
router.use('/maintenancereport', mrRouter)

module.exports=router