require('dotenv').config()
const PORT =process.env.PORT || 3001
const express= require("express")
const bodyParser = require('body-parser');
const router = require('./routes/index')
//const knex = require('./db/db')knex(knexConfig[process.env.NODE_ENV])const knexConfig = require('./db/knexfile'); 
const app=express()
var cors = require('cors');
app.use(cors())

app.get('/', (_, res) => {
  res.json({ msg: 'CORS включен для всех запросов!' })
})


app.use(bodyParser.json()); 
app.use(express.json());
app.use(router);

app.get('/', (req, res) => {
    res.send('Hello World!');
});

app.listen(PORT,()=>
{
    console.log('Server starting on port '+PORT)
})