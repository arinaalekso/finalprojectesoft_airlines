const knex = require('knex')

exports.up = function (knex) {
    return knex.schema.createTable('aircraft', function (table) {
        table.increments('id').primary()
        table.string('vid', 255).notNullable()
        table.string('types', 255).notNullable()
        table.string('classes', 255).notNullable()
        table.integer('year_of_release').notNullable()
        table.string('volume_of_traffic',255).notNullable()
        table.timestamp('date_lasttech').notNullable()
        table.string('specified_life',255).notNullable() 
    })
}

exports.down = function (knex) {
    return knex.schema.dropTable('aircraft')
}