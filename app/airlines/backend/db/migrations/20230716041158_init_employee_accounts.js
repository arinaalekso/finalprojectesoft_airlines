const knex = require('knex')

exports.up = function (knex) {
    return knex.schema.createTable('employee_accounts', function (table) {
        table.integer('idEmployees').unsigned().notNullable()
        table
            .foreign('idEmployees')
            .references('id')
            .inTable('employees')
            .onDelete('Cascade')
            .onUpdate('Cascade')
        table.string('login', 255).unique().notNullable()
        table.string('password', 255).notNullable()
    })
}

exports.down = function (knex) {
    return knex.schema.dropTable('accounts')
}