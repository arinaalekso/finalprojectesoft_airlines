const knex = require('knex')

exports.up = function (knex) {
    return knex.schema.createTable('flight', function (table) {
        table.increments('id').primary()
        table.integer('id_aircraft').notNullable()
        table.foreign('id_aircraft').references('id').inTable('aircraft')
        table.integer('id_first_pilot').notNullable()
        table.foreign('id_first_pilot').references('id').inTable('employees')
        table.integer('id_second_pilot').notNullable()
        table.foreign('id_second_pilot').references('id').inTable('employees')
        table.integer('id_eng').notNullable()
        table.foreign('id_eng').references('id').inTable('employees')
        table.timestamp('data_flight').notNullable()
        table.string('cargo_type', 255).notNullable()
        table.string('flight_route', 255).notNullable()
        table.string('time_on_flight', 255).notNullable()
    })
}

exports.down = function (knex) {
    return knex.schema.dropTable('flight')
}