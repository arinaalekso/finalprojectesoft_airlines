const knex = require('knex')

exports.up = function (knex) {
    return knex.schema.createTable('employees', function (table) {
        table.increments('id').primary()
        table.string('surname', 255).notNullable()
        table.string('name', 255).notNullable()
        table.string('patronymic', 255).notNullable()
        table.timestamp('dr').notNullable()
        table.string('phonenumber', 255).notNullable()
        table.string('email', 255).notNullable()
        table.string('post', 255).notNullable()
        table.string('department', 255).notNullable()
        table.string('diploma', 255).notNullable()
        table.timestamp('date_empl').notNullable()
    })
}

exports.down = function (knex) {
    return knex.schema.dropTable('employees')
}