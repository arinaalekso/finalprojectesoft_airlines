const knex = require('knex')

exports.up = function (knex) {
    return knex.schema.createTable('mr', function (table) {
        table.increments('id').primary()
        table.string('act_number', 255).notNullable()
        table.timestamp('date_act').notNullable()
        table.integer('id_aircraft').notNullable()
        table.foreign('id_aircraft').references('id').inTable('aircraft')
        table.integer('ID_master_eng').notNullable()
        table.foreign('ID_master_eng').references('id').inTable('employees')
        table.integer('ID_first_eng').notNullable()
        table.foreign('ID_first_eng').references('id').inTable('employees')
        table.integer('id_second_eng').notNullable()
        table.foreign('id_second_eng').references('id').inTable('employees')
        table.integer('ID_first_tech').notNullable()
        table.foreign('ID_first_tech').references('id').inTable('employees')
        table.integer('ID_second_tech').notNullable()
        table.foreign('ID_second_tech').references('id').inTable('employees')
        table.timestamp('date_tech').notNullable()
        table.string('reason', 255).notNullable()
        table.string('result', 255).notNullable()
    })
}

exports.down = function (knex) {
    return knex.schema.dropTable('mr')
}