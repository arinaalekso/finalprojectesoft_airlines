exports.seed = function(knex) {
  return knex('employees').del()
    .then(function () { 
      return knex('employees').insert([
        {id: 1, surname: 'Петров', name: 'Петр', patronymic:'Петрович',dr:'28-02-1990', phonenumber:'89512334567', email:'petrov_p_p@gmail.com',post:'Инженер',department:'Инженерно-Технический Отдел',diploma:'213456 0987654',date_empl:'09-02-2015'},
        {id: 2, surname: 'Иванов', name: 'Иван', patronymic:'Иванович',dr:'20-01-1970', phonenumber:'89502324547', email:'ivanov_i_i@gmail.com',post:'Ведущий инженер',department:'Инженерно-Технический Отдел',diploma:'223056 0987254',date_empl:'12-04-2000'},
        {id: 3, surname: 'Иванов', name: 'Петр', patronymic:'Андреевич',dr:'08-06-1980', phonenumber:'89500334567', email:'ivanov_p_a@gmail.com',post:'Летчик',department:'Летная Служба',diploma:'913456 0987654',date_empl:'09-02-2009'},
        {id: 4, surname: 'Романов', name: 'Андрей', patronymic:'Андреевич',dr:'21-06-1971', phonenumber:'89500554567', email:'romanov_a_a@gmail.com',post:'Летчик',department:'Летная Служба',diploma:'913111 0987654',date_empl:'19-02-1995'},
        {id: 5, surname: 'Копылов', name: 'Матвей', patronymic:'Артемович',dr:'24-09-1987', phonenumber:'89512990567', email:'kopylov_m_a@gmail.com',post:'Инженер',department:'Инженерно-Технический Отдел',diploma:'210056 8900654',date_empl:'09-02-2015'},
        {id: 6, surname: 'Кудрявцев', name: 'Алан', patronymic:'Иосифович',dr:'28-02-1990', phonenumber:'89512334567', email:'kudryavtsev_a_i@gmail.com',post:'Техник',department:'Инженерно-Технический Отдел',diploma:'213456 0987654',date_empl:'09-02-2015'},
        {id: 7, surname: 'Силин', name: 'Тихон', patronymic:'Станиславович',dr:'28-02-1990', phonenumber:'89776543210', email:'silin_ts@gmail.com',post:'Техник',department:'Инженерно-Технический Отдел',diploma:'546456 3214567',date_empl:'09-02-2015'}
      ]);
    });
};