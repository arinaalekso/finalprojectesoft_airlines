exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('aircraft').del()
    .then(function () {
      // Inserts seed entries
      return knex('aircraft').insert([
        {id: 1, vid: 'Ми-8П', types: 'вертолёт', classes:'пассажирский',volume_of_traffic:'28 мест', year_of_release:2016, date_lasttech:'12-05-2023',specified_life:'12000 лётных часов'},
        {id: 2, vid: 'Airbus А380', types: 'самолёт', classes:'пассажирский',volume_of_traffic:'1378 мест', year_of_release:2019, date_lasttech:'29-06-2023',specified_life:'42000 лётных часов'},
        {id: 3, vid: 'Ан-158', types: 'самолёт', classes:'пассажирский',volume_of_traffic:'100 мест', year_of_release:2017, date_lasttech:'19-04-2023',specified_life:'26000 лётных часов'}
      ]);
    });
};
