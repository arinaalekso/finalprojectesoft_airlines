exports.seed = function(knex) {
  return knex('flight').del()
    .then(function () { 
      return knex('flight').insert([
        {id: 1, id_aircraft: 1, id_first_pilot: 3, id_second_pilot:4,id_eng:1, data_flight:'13-06-2023', cargo_type:'Пассажиры',flight_route:'Белоярский-Тюмень',time_on_flight:'3 ч 35 минут '}
      ]);
    });
};