exports.seed = function(knex) {
  return knex('employee_accounts').del()
    .then(function () { 
      return knex('employee_accounts').insert([
        {idEmployees: 1, login: 'petrov_pp', password: 'blabla1233'},
        {idEmployees: 2, login: 'ivanov_i_i', password: 'ivanov09876'},
        {idEmployees: 3, login:'ivanov_p_a', password: 'hell0_1234'},
        {idEmployees: 4, login:'romanov_a_a', password: 'romanov_4a_a'},
        {idEmployees: 5, login: 'kopylov_m_a', password:'kopylov_m9_a8'},
        {idEmployees: 6, login:'kudryavtsev_a_i', password:'kudryavtsev_a_i'},
        {idEmployees: 7, login:'silin_ts', password:'silin_t8s8'}
      ]);
    });
};
