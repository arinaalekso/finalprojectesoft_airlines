const donenv = require('dotenv').config()

const password = (module.exports = {
    development: {
        client: 'pg',
        connection: {
            host: '127.0.0.1',
            port: 5432,
            user: 'postgres', 
            password: 'root',
            database: 'airlines',
        },
        migrations: {
            directory: 'migrations',
            tableName: 'knex_migrations',
        },
        seeds: {
            directory: 'seeds',
        },
    },
})