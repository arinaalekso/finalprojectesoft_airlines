const knexConfig = require('../db/knexfile');
const knex = require('knex')(knexConfig.development);

class MRControllers
{
    getShow(req, res)
    {
        knex.select()
            .table('mr').then((mr)=>
            {
                res.send(mr);
            })
    }
}

module.exports = new MRControllers;