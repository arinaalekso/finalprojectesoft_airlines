const knexConfig = require('../db/knexfile');
const knex = require('knex')(knexConfig.development);

class FlightPersonalControllers
{
    getShow(req, res)
    {
        knex.select().table('employees').where('department', "Летная Служба").then((employees)=>
            {
                res.send(employees);
            })
    }
}

module.exports = new FlightPersonalControllers;