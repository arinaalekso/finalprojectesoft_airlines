const knexConfig = require('../db/knexfile');
const knex = require('knex')(knexConfig.development);

class FlightControllers
{
    getShow(req, res)
    {
        knex.select()
            .table('flight').then((flight)=>
            {
                res.send(flight);
            })
    }
}

module.exports = new FlightControllers;