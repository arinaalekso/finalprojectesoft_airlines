const knexConfig = require('../db/knexfile');
const knex = require('knex')(knexConfig.development);

class AircraftControllers
{
    getShow(req, res)
    {
        knex.select().orderBy('id')
            .table('aircraft').then((aircraft)=>
            {
                res.send(aircraft);
            })
    }
}

module.exports = new AircraftControllers;
