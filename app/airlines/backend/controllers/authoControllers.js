const knexConfig = require('../db/knexfile');
const knex = require('knex')(knexConfig.development);
const bcrypt = require('bcrypt');

class AuthoControllers
{
    registration(req,res)
    {
        //const {id_emp} = req.query;
        const {surname_emp,name_emp,patronymic_emp,dr_emp,phonenumber_emp,email_emp,post_emp,department_emp,diploma_emp,date_empl_emp,login_emp,password_emp}=req.body;
        knex('employee_accounts')
            .where('login', login_emp)
            .then(rows => {
                if (rows.length) 
                {
                    res.status(409).send('Аккаунт с таким логином уже существует! Придумайте, пожалуйста, другой логин.');
                } 
                else 
                {
                    bcrypt.hash(password_emp, 10, (err, hash) => {
                        knex('employees')
                            .insert({
                                surname: surname_emp, 
                                name: name_emp, 
                                patronymic:patronymic_emp,
                                dr:dr_emp,
                                phonenumber: phonenumber_emp,
                                email: email_emp,
                                post: post_emp,
                                department: department_emp,
                                diploma:diploma_emp,
                                date_empl:date_empl_emp
                            })
                            .increment('id', 1)
                            .then(()=>{
                                idEmp_acc=knex('employees').count('id');
                                knex('employee_accounts')
                                    .insert({idEmployees: idEmp_acc, login: login_emp, password: hash })
                                    .then(() => {
                                        res.status(201).send('Поздравляю. Аккаунт успешно создан!')
                                    });
                            })
                        
                    });
                };
            })
            .catch(err => {
                res.status(500).send(err);
            })
    }

    login(req, res) 
    {
        const { login_user, password_user } = req.body;
        knex('employee_accounts')
            .where('login', login_user)
            .select('idEmployees', 'password')
            .then(async rows => {
                if (rows.length) 
                {
                    if (await bcrypt.compare(password_user, rows[0].password)) 
                    {
                        /* const { access_token, refresh_token } = TokenServ.generateTokens({ id: rows[0].id, role: rows[0].role});
                        res.cookie('refresh_token', refresh_token, {
                            httpOnly: true,
                            sameSite: 'strict',
                            maxAge: 30 * 24 * 60 * 60 * 1000
                        })
                            .cookie('access_token', access_token, {
                                httpOnly: true,
                                sameSite: 'strict',
                                maxAge: 15 * 60 * 1000
                            });
                        return res.status(200).json({ id: rows[0].id, role: rows[0].role}); */
                    } 
                    else 
                    {
                        res.status(402).send(`Неверный пароль`);
                    } 
                } 
                else 
                {
                    res.status(401).send(`Неверный email`);
                }
            })
            .catch(err => {
                res.status(500).send(err);
            })
    };
}

module.exports = new AuthoControllers();