const knexConfig = require('../db/knexfile');
const knex = require('knex')(knexConfig.development);

class EngTechControllers
{
    getShow(req, res)
    {
        knex.select().table('employees').where('department', "Инженерно-Технический Отдел").then((employees)=>
            {
                res.send(employees);
            })
    }
}

module.exports = new EngTechControllers;