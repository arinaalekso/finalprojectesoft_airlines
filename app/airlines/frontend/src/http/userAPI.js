import {$authHost, $host} from "./index";
import jwt_decode from "jwt-decode";

export const registration = async (login, password) => {
    const {data} = await $host.post('autho', {login, password})
    localStorage.setItem('token', data.token)
    return jwt_decode(data.token)
}

export const LoginCheck = async (login, password) => {
    const {data} = await $host.post('api/autho', {login, password})
    localStorage.setItem('token', data.token)
    return jwt_decode(data.token)
}

/**/ export const check = async () => {
    const {data} = await $authHost.post('/autho')
    localStorage.setItem('token', data.token)
    return jwt_decode(data.token)
} 