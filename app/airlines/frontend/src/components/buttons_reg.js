import React, { useState } from 'react';
import Modal from './modal_windows';
import './css/autho.css'; 

function ButtonReg()
{
    const[modal,SetModal]=useState({
        modal1: false 
    })

    const modal_reg=(
        <form>
            <div className="reg">
                <p>
                    <span id="text_label">Фамилия</span>
                    <input type="text" name="surname" placeholder="Фамилия"/>
                </p>  
                <p className="com1"></p>
                <p>
                    <span id="text_label"> Имя</span>
                    <input type="text" name="name" placeholder="Имя"/>
                </p>  
                <p className="com1"></p>
                <p>
                    <span id="text_label">Отчество</span>
                    <input type="text" name="patronymic" placeholder="Отчество"/>
                </p>  
                <p className="com1"></p>
                <p>
                    <span id="text_label">Дата рождения</span>
                    <input type="text" name="dr" placeholder="Дата рождения"/>
                </p>  
                <p className="com1"></p> 
                <p>
                    <span id="text_label">Номер телефона</span>
                    <input type="text" name="phonenumber" placeholder="Номер телефона"/>
                </p>  
                <p className="com1"></p>
                <p>
                    <span id="text_label"> Эл. Почта </span>
                    <input type="text" name="email" placeholder="Эл. Почта"/>
                </p>  
                <p className="com1"></p>
                <p>
                    <span id="text_label"> Должность </span>
                    <input type="text" name="post" placeholder="Должность"/>
                </p>  
                <p className="com1"></p>
                <p>
                    <span id="text_label"> Отдел </span>
                    <input type="text" name="department" placeholder="Отдел"/>
                </p>  
                <p className="com1"></p>
                <p>
                    <span id="text_label"> Диплом </span>
                    <input type="text" name="diploma" placeholder="Диплом"/>
                </p>  
                <p className="com1"></p>
                <p>
                    <span id="text_label"> Дата устройства на работу  </span>
                    <input type="text" name="date_empl" placeholder="Дата устройства на работу"/>
                </p>  
                <p className="com1"></p>
                <p>
                    <span id="text_label"> Логин </span>
                    <input type="text" name="login" placeholder="Логин"/>
                </p>  
                <p className="com1"></p>
                <p>
                    <span id="text_label"> Пароль </span>
                    <input type="text" name="password" placeholder="Пароль"/>
                </p>
                <p className="com2"></p>
                <button className='btn_r'name="reges"> ЗАРЕГИСТРИРОВАТЬСЯ </button> 
            </div>            
        </form>
    );

    return(
        <div className="btn_reg"> 
            <button onClick={()=> SetModal({...modal, modal1:true})}>РЕГИСТРАЦИЯ</button>
            <Modal title="РЕГИСТРАЦИЯ" 
                isOpened={modal.modal1} 
                onModalClose={()=>SetModal({...modal, modal1:false})}>
                {modal_reg}
            </Modal>   
        </div>
    );
}

export default ButtonReg;