import React,{useEffect,useState} from "react";

function AircraftData(props) 
{
    const { aircraft } = props;
    console.log(aircraft);
    var options = {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
        timezone: 'UTC'
    }; 
    if (!aircraft || aircraft.length === 0) return <p>Нет данных.</p>

    return (
        <div className="formas">
            <table>
                <thead>
                    <tr>
                        <th id="row_id">ID</th>
                        <th>Вид</th>
                        <th>Тип</th>
                        <th>Класс</th>
                        <th>Объем перевозок</th>
                        <th>Год выпуска</th>
                        <th>Дата крайнего технического обслуживания</th>
                        <th>Назначенный ресурс</th> 
                    </tr>
                </thead>
                <tbody>
                    {
                        aircraft.map((aircraft) =>
                            <tr key={aircraft.id}>
                                <td id="row_id">{aircraft.id}</td>
                                <td>{aircraft.vid}</td>
                                <td>{aircraft.types}</td>
                                <td>{aircraft.classes}</td>
                                <td>{aircraft.volume_of_traffic}</td>
                                <td>{new Date(aircraft.date_lasttech).toLocaleString("ru", options)}</td>
                                <td>{aircraft.specified_life}</td>
                                <td>{aircraft.year_of_release}</td>
                            </tr>
                        )
                    }
                </tbody>
            </table>
        </div>
    )
}



export default AircraftData;