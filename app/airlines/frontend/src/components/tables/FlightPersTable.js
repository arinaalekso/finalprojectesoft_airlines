import React,{useEffect,useState} from "react";

function FlightPersData(props) 
{
    const { flightpersonal } = props; 
    var options = {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
        timezone: 'UTC'
    }; 
    var i=0;
    if (!flightpersonal || flightpersonal.length === 0) return <p>Нет данных.</p>

    return (
        <div className="formas">
            <table>
                <thead>
                    <tr>
                        <th id="row_id">ID</th>
                        <th>Фамилия</th>
                        <th>Имя</th>
                        <th>Отчество</th>
                        <th>Дата рождения</th>
                        <th>Номер Телефона </th>
                        <th>Email</th>
                        <th>Должность</th>
                        <th>Диплом</th>
                        <th>Дата устройства на работу</th>  
                    </tr>
                </thead>
                <tbody>
                    {
                        flightpersonal.map((flightpersonal) =>
                            <tr key={flightpersonal.id}>
                                <td id="row_id">{i=i+1}</td>
                                <td>{flightpersonal.surname}</td>
                                <td>{flightpersonal.name}</td>
                                <td>{flightpersonal.patronymic}</td>
                                <td>{new Date(flightpersonal.dr).toLocaleString("ru", options)}</td>
                                <td>{flightpersonal.phonenumber}</td>
                                <td>{flightpersonal.email}</td>
                                <td>{flightpersonal.post}</td>
                                <td>{flightpersonal.diploma}</td>
                                <td>{new Date(flightpersonal.date_empl).toLocaleString("ru", options)}</td> 
                            </tr>
                        )
                    }
                </tbody>
            </table>
        </div>
    )
}

export default FlightPersData;