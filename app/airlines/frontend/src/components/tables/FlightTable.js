import React,{useEffect,useState} from "react";

function FlightData(props) 
{
    const {flight} = props;
    console.log(flight);
    var options = {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
        timezone: 'UTC'
    }; 
    if (!flight || flight.length === 0) return <p>Нет данных.</p>

    return (
        <div className="formas">
            <table>
                <thead>
                    <tr>
                        <th id="row_id">ID</th>
                        <th>Воздушное судно</th>
                        <th>ФИО Первого летчика</th>
                        <th>ФИО Второго летчика</th>
                        <th>ФИО Инженера</th>
                        <th>Дата рейса</th>
                        <th>Вид груза</th>
                        <th>Маршрут рейса</th>
                        <th>Летное время в рейсе</th>
                    </tr>
                </thead>
                <tbody>
                {
                    flight.map((flight) =>
                        <tr id="row_id" key={flight.id}>
                            <td>{flight.id}</td>
                            <td>{flight.id_aircraft}</td>
                            <td>{flight.id_first_pilot}</td>
                            <td>{flight.id_second_pilot}</td>                        
                            <td>{flight.id_eng}</td>
                            <td>{new Date(flight.data_flight).toLocaleString("ru", options)}</td>
                            <td>{flight.cargo_type}</td>
                            <td>{flight.flight_route}</td>
                            <td>{flight.time_on_flight}</td>
                        </tr>
                    )
                }
                </tbody>
            </table>
        </div>
    )
}



export default FlightData;