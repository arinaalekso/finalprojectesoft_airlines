import React,{useEffect,useState} from "react";

function EngTechData(props) 
{
    const { engtech } = props;
    var options = {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
        timezone: 'UTC'
    }; 
    var i=0;
    if (!engtech || engtech.length === 0) return <p>Нет данных.</p>

    return (
        <div className="formas">
            <table>
                <thead>
                    <tr>
                        <th id="row_id">№</th>
                        <th>Фамилия</th>
                        <th>Имя</th>
                        <th>Отчество</th>
                        <th>Дата рождения</th>
                        <th>Номер Телефона </th>
                        <th>Email</th>
                        <th>Должность</th>
                        <th>Диплом</th>
                        <th>Дата устройства на работу</th>  
                    </tr>
                </thead>
                <tbody>
                {
                    engtech.map((engtech) =>
                        <tr key={engtech.id}>
                            <td id="row_id">{i=i+1}</td>
                            <td>{engtech.surname}</td>
                            <td>{engtech.name}</td>
                            <td>{engtech.patronymic}</td>
                            <td>{new Date(engtech.dr).toLocaleString("ru", options)}</td>
                            <td>{engtech.phonenumber}</td>
                            <td>{engtech.email}</td>
                            <td>{engtech.post}</td>
                            <td>{engtech.diploma}</td>
                            <td>{new Date(engtech.date_empl).toLocaleString("ru", options)}</td> 
                        </tr>
                    )
                }
                </tbody>
            </table>
        </div>
    )
}



export default EngTechData;