import React,{useEffect,useState} from "react";

function MRData(props) 
{
    const {mr} = props; 
    var options = {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
        timezone: 'UTC'
    }; 
    if (!mr || mr.length === 0) return <p>Нет данных.</p>

    return (
        <div className="formas">
            <table>
                <thead>
                    <tr>
                        <th id="row_id">ID</th>
                        <th>Номер акта</th>
                        <th>Дата составления акта</th>
                        <th>Воздушное судно</th> 
                        <th>ФИО Главного инженера</th>
                        <th>ФИО Первого инженера</th>
                        <th>ФИО Второго инженера</th>
                        <th>ФИО Первого техника</th>
                        <th>ФИО Второго техника</th>
                        <th>Дата технического обслуживания</th> 
                        <th>Причина технического обслуживания</th>
                        <th>Результат</th>
                    </tr>
                </thead>
                <tbody>
                {
                    mr.map((mr) =>
                        <tr id="row_id" key={mr.id}>
                            <td>{mr.id}</td>
                            <td>{mr.act_number}</td>
                            <td>{new Date(mr.date_act).toLocaleString("ru", options)}</td>
                            <td>{mr.id_aircraft}</td> 
                            <td>{mr.ID_master_eng}</td>
                            <td>{mr.ID_first_eng}</td>
                            <td>{mr.ID_second_eng}</td>
                            <td>{mr.ID_first_tech}</td>
                            <td>{mr.ID_second_tech}</td>
                            <td>{new Date(mr.date_tech).toLocaleString("ru", options)}</td> 
                            <td>{mr.reason}</td>
                            <td>{mr.result}</td>
                        </tr>
                    )
                }
                </tbody>
            </table>
        </div>
    )
}

export default MRData;