import React, { useState,useEffect } from 'react';
import Modal from './modal_windows';
import '../components/css/add_edit_info.css';  
import ID_EngTechSelect from './selectes/ID_EngTechSelect';
import OnLoadingData from "./LoadingDataModal";
import axios from "axios";

const apiUrl_engtech = 'http://localhost:3001/eng_tech';

function ButtonET_edits()
{
    const DataLoading =  OnLoadingData(ID_EngTechSelect);
    const [engtechState, setEngTechState] = useState(
    {
        loading: false,
        id_engtech: null,
    })
    useEffect(() => {
        setEngTechState({loading: true})
        axios.get(apiUrl_engtech).then((resp) => {
            const allEngTech = resp.data;
            setEngTechState({
                loading: false,
                id_engtech: allEngTech
            });
        });
    }, [setEngTechState]);

    const[modal,SetModal]=useState({
        modal1: false,
        modal2: false,
        modal3: false
    })

    const modal_add=(
        <form method="POST" action="">
            <div className="formas_modal">
                <p> 
                    <span id="text_label">Фамилия</span>
                    <input type="text" name="surname" placeholder="Фамилия"/> 
                </p>  
                <p class="com1"></p>
                <p>
                    <span id="text_label"> Имя</span> 
                    <input type="text" name="name" placeholder="Имя"/>
                </p>  
                <p class="com1"></p>
                <p>
                    <span id="text_label">Отчество</span> 
                    <input type="text" name="patronymic" placeholder="Отчество"/> 
                </p>  
                <p class="com1"></p>
                <p>
                    <span id="text_label">Дата рождения</span>
                    <input type="text" name="dr" placeholder="Дата рождения"/>
                </p>  
                <p class="com1"></p> 
                <p>
                    <span id="text_label">Номер телефона</span>
                    <input type="text" name="phonenumber" placeholder="Номер телефона"/>
                </p>  
                <p class="com1"></p>
                <p>
                    <span id="text_label">Эл. Почта </span>
                    <input type="text" name="email" placeholder="Эл. Почта"/> 
                </p>  
                <p class="com1"></p>
                <p> 
                    <span id="text_label">Должность </span>
                    <input type="text" name="post" placeholder="Должность"/> 
                </p>  
                <p class="com1"></p>
                <p> 
                    <span id="text_label">Отдел </span>  
                    <input type="text" name="department" placeholder="Отдел"/> 
                </p>  
                <p class="com1"></p>
                <p> 
                    <span id="text_label">Диплом </span>  
                    <input type="text" name="diploma" placeholder="Диплом"/> 
                </p>  
                <p class="com1"></p>
                <p> 
                    <span id="text_label">Дата устройства на работу </span>
                    <input type="text" name="date_empl" placeholder="Дата устройства на работу"/>
                </p>  
                <p class="com1"></p> 
                <input type="submit" value="СОХРАНИТЬ" name="save"/>
            </div>            
        </form>
    );

    const modal_edit = (
        <form method="POST" action="">
            <div className="formas_modal">
                <p> 
                    <span id="text_label">ID</span> 
                    <DataLoading isLoading={engtechState.loading} id_engtech={engtechState.id_engtech} />
                    <button name="search" class="searchs"> Найти </button>
                </p>  
                <p class="com1"></p>
                <p>
                    <span id="text_label">Фамилия</span>
                    <input type="text" name="surname" placeholder="Фамилия"/>
                </p>  
                <p class="com1"></p>
                <p>
                    <span id="text_label"> Имя</span>
                    <input type="text" name="name" placeholder="Имя"/> 
                </p>  
                <p class="com1"></p>
                <p>
                    <span id="text_label">Отчество</span> 
                    <input type="text" name="patronymic" placeholder="Отчество"/>
                </p>  
                <p class="com1"></p>
                <p>
                    <span id="text_label">Дата рождения</span>
                    <input type="text" name="dr" placeholder="Дата рождения"/>
                </p>  
                <p class="com1"></p> 
                <p>
                    <span id="text_label">Номер телефона</span>
                    <input type="text" name="phonenumber" placeholder="Номер телефона"/>
                </p>  
                <p class="com1"></p>
                <p>
                    <span id="text_label"> Эл. Почта </span>
                    <input type="text" name="email" placeholder="Эл. Почта"/>
                </p>  
                <p class="com1"></p>
                <p>
                    <span id="text_label"> Должность </span>
                    <input type="text" name="post" placeholder="Должность"/>
                </p>  
                <p class="com1"></p>
                <p>
                    <span id="text_label"> Отдел </span>
                    <input type="text" name="department" placeholder="Отдел"/>
                </p>  
                <p class="com1"></p>
                <p>
                    <span id="text_label"> Диплом </span>
                    <input type="text" name="diploma" placeholder="Диплом"/>
                </p>  
                <p class="com1"></p>
                <p>
                    <span id="text_label"> Дата устройства на работу  </span>
                    <input type="text" name="date_empl" placeholder="Дата устройства на работу"/>
                </p>  
                <p class="com1"></p> 
                <input type="submit" value="СОХРАНИТЬ" name="save"/>   
            </div>              
        </form>     
    );

    const modal_delete = ( 
        <form method="POST" action="">
            <div class="formas_modal">
                <p>
                    <span id="text_label">ID</span>
                    <input type="text" name="id" class="del_info" placeholder="ID"/>
                </p>  
                <p class="com1"></p>
                <input type="submit" class="btn_del_info"  value="СОХРАНИТЬ" name="save"/>   
            </div>                
        </form>
    ); 

    return(
        <div id="buts"> 
            <button class="but_setting" id="but_add" name='add' onClick={()=> SetModal({...modal, modal1:true})}>
                Добавить 
            </button>
            <Modal title="Добавить информацию" 
                isOpened={modal.modal1} 
                onModalClose={()=>SetModal({...modal, modal1:false})}>
                {modal_add}
            </Modal>   
            <button class="but_setting" id="but_edit" name='edits_info'  onClick={()=> SetModal({...modal, modal2:true})}> 
                Изменить
            </button>
            <Modal title="Изменить информацию" 
                isOpened={modal.modal2} 
                onModalClose={()=>SetModal({...modal, modal2:false})}>
                {modal_edit}    
            </Modal>
            <button class="but_setting" id="but_del" name='del' onClick={()=> SetModal({...modal, modal3:true})}>
                Удалить
            </button>
            <Modal title="Удалить информацию" 
                isOpened={modal.modal3} 
                onModalClose={()=>SetModal({...modal, modal3:false})}>
                {modal_delete}    
            </Modal>
        </div>
    );
}

export default ButtonET_edits;