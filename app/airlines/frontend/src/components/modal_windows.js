import {IoClose} from 'react-icons/io5';

const Modal = props =>{

    return(
        <div className={`modal__wrapper ${props.isOpened ? 'open':'close'}`} style={{...props.style}}>
            <div className="forma_modal" id='modal_forma'>
                <div className='btn_close' onClick={props.onModalClose} >
                    <IoClose /> 
                </div>    
                <h1> {props.title}</h1>
                {props.children}
            </div>
        </div>
    )
}

export default Modal