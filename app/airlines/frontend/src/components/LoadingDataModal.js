function OnLoadingData(Component) {
    return function LoadingDatas({ isLoading, ...props }) {
        if (!isLoading) return <Component {...props} />

        else return (
            <div>
                <p>Подождите, данные загружаются!</p>
            </div>
        )
    }
}

export default OnLoadingData;