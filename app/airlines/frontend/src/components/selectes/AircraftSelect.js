import React,{useEffect,useState} from "react";

function AircraftSelect(props) 
{
    const { id_aircraft } = props; 
    if (!id_aircraft || id_aircraft.length === 0) return <p>Нет данных.</p>

    return (
        <select id="selectID" name="Id_aircraft" placeholder="ID">
            {
                id_aircraft.map((id_aircraft) =>
                    <option value={id_aircraft.id}>{id_aircraft.id}</option>
                )
            }
        </select>
    )
}



export default AircraftSelect;