import React,{useEffect,useState} from "react";

function ID_FirstFlightPersSelect(props) 
{
    const { id_firstflightpers } = props; 
    if (!id_firstflightpers || id_firstflightpers.length === 0) return <p>Нет данных.</p>

    return (
        <select id="selectID" name="Id_firstfp" placeholder="ID">
            {
                id_firstflightpers.map((id_firstflightpers) =>
                    <option value={id_firstflightpers.id}>{id_firstflightpers.id}</option>
                )
            }
        </select>
    )
}

export default ID_FirstFlightPersSelect;