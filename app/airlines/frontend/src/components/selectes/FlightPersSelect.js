import React,{useEffect,useState} from "react";

function FlightPersSelect(props) 
{
    const { id_flightpers } = props; 
    if (!id_flightpers || id_flightpers.length === 0) return <p>Нет данных.</p>

    return (
        <select id="selectID" name="Id_fp" placeholder="ID">
            {
                id_flightpers.map((id_flightpers) =>
                    <option value={id_flightpers.id}>{id_flightpers.id}</option>
                )
            }
        </select>
    )
}

export default FlightPersSelect;