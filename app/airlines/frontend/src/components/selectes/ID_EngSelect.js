import React,{useEffect,useState} from "react";

export default function ID_EngSelect(props) 
{
    const { id_eng } = props; 
    if (!id_eng || id_eng.length === 0) return <p>Нет данных.</p>

    return (
        <select id="selectID" name="Id_eng" placeholder="ID">
            {
                id_eng.map((id_eng) =>
                    <option value={id_eng.id}>{id_eng.id}</option>
                )
            }
        </select>
    )
}
