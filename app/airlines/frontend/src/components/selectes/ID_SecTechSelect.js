import React,{useEffect,useState} from "react";

export default function ID_SecTechSelect(props) 
{
    const { id_sectech } = props; 
    if (!id_sectech || id_sectech.length === 0) return <p>Нет данных.</p>

    return (
        <select id="selectID" name="Id_secstech" placeholder="ID">
            {
                id_sectech.map((id_sectech) =>
                    <option value={id_sectech.id}>{id_sectech.id}</option>
                )
            }
        </select>
    )
}