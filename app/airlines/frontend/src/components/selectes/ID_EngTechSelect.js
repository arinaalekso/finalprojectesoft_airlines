import React,{useEffect,useState} from "react";

export default function ID_EngTechSelect(props) 
{
    const { id_engtech } = props; 
    if (!id_engtech || id_engtech.length === 0) return <p>Нет данных.</p>

    return (
        <select id="selectID" name="Id_engtech" placeholder="ID">
            {
                id_engtech.map((id_engtech) =>
                    <option value={id_engtech.id}>{id_engtech.id}</option>
                )
            }
        </select>
    )
}