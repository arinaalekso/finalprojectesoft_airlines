import React,{useEffect,useState} from "react";

export default function ID_FirstEngSelect(props) 
{
    const { id_firsteng } = props; 
    if (!id_firsteng || id_firsteng.length === 0) return <p>Нет данных.</p>

    return (
        <select id="selectID" name="Id_firsteng" placeholder="ID">
            {
                id_firsteng.map((id_firsteng) =>
                    <option value={id_firsteng.id}>{id_firsteng.id}</option>
                )
            }
        </select>
    )
}