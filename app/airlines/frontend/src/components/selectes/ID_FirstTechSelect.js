import React,{useEffect,useState} from "react";

export default function ID_FirstTechSelect(props) 
{
    const { id_firsttech } = props; 
    if (!id_firsttech || id_firsttech.length === 0) return <p>Нет данных.</p>

    return (
        <select id="selectID" name="Id_firsttech" placeholder="ID">
            {
                id_firsttech.map((id_firsttech) =>
                    <option value={id_firsttech.id}>{id_firsttech.id}</option>
                )
            }
        </select>
    )
}
