import React,{useEffect,useState} from "react";

function MRSelect(props) 
{
    const { id_mr } = props; 
    if (!id_mr || id_mr.length === 0) return <p>Нет данных.</p>

    return (
        <select id="selectID" name="Id_fp" placeholder="ID">
            {
                id_mr.map((id_mr) =>
                    <option value={id_mr.id}>{id_mr.id}</option>
                )
            }
        </select>
    )
}

export default MRSelect;