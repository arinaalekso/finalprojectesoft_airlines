import React,{useEffect,useState} from "react";

export default function ID_SecEngSelect(props) 
{
    const { id_seceng } = props; 
    if (!id_seceng || id_seceng.length === 0) return <p>Нет данных.</p>

    return (
        <select id="selectID" name="Id_secseng" placeholder="ID">
            {
                id_seceng.map((id_seceng) =>
                    <option value={id_seceng.id}>{id_seceng.id}</option>
                )
            }
        </select>
    )
}