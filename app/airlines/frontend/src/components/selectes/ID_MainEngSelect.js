import React,{useEffect,useState} from "react";

export default function ID_MainEngSelect(props) 
{
    const { id_maineng } = props; 
    if (!id_maineng || id_maineng.length === 0) return <p>Нет данных.</p>

    return (
        <select id="selectID" name="Id_maineng" placeholder="ID">
            {
                id_maineng.map((id_maineng) =>
                    <option value={id_maineng.id}>{id_maineng.id}</option>
                )
            }
        </select>
    )
}
