import React,{useEffect,useState} from "react";

function FlightSelect(props) 
{
    const { id_flight } = props; 
    if (!id_flight || id_flight.length === 0) return <p>Нет данных.</p>

    return (
        <select id="selectID" name="Id_f" placeholder="ID">
            {
                id_flight.map((id_flight) =>
                    <option value={id_flight.id}>{id_flight.id}</option>
                )
            }
        </select>
    )
}

export default FlightSelect;