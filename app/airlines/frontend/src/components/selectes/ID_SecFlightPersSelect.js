import React,{useEffect,useState} from "react";

function ID_SecFlightPersSelect(props) 
{
    const { id_secflightpers } = props; 
    if (!id_secflightpers || id_secflightpers.length === 0) return <p>Нет данных.</p>

    return (
        <select id="selectID" name="Id_secfp" placeholder="ID">
            {
                id_secflightpers.map((id_secflightpers) =>
                    <option value={id_secflightpers.id}>{id_secflightpers.id}</option>
                )
            }
        </select>
    )
}

export default ID_SecFlightPersSelect;