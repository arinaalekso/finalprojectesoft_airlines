import React, { useState, useEffect } from 'react';
import Modal from './modal_windows';
import '../components/css/add_edit_info.css'; 
import AircraftSelect from "./selectes/AircraftSelect";
import ID_EngSelect from "./selectes/ID_EngSelect";
import OnLoadingData from "./LoadingDataModal";
import axios from "axios";
import ID_MainEngSelect from './selectes/ID_MainEngSelect';
import ID_FirstEngSelect from './selectes/ID_FirstEngSelect';
import ID_SecEngSelect from './selectes/ID_SecEngSelect';
import ID_FirstTechSelect from './selectes/ID_FirstTechSelect';
import ID_SecTechSelect from './selectes/ID_SecTechSelect';

const apiUrl_aircraft = 'http://localhost:3001/aircraft';
const apiUrl_engtech = 'http://localhost:3001/eng_tech';

function ButtonMA_edits()
{
    const DataLoading_Aircraft =  OnLoadingData(AircraftSelect);
    const [aircraftState, setAircraftState] = useState(
    {
        loading_aircraft: false,
        id_aircraft: null,
    })
    useEffect(() => {
        setAircraftState({loading_aircraft: true})
        axios.get(apiUrl_aircraft).then((resp) => {
            const allAircraft = resp.data;
            setAircraftState({
                loading_aircraft: false,
                id_aircraft: allAircraft
            });
        });
    }, [setAircraftState]);

    const DataLoading_FirstEng =  OnLoadingData(ID_FirstEngSelect);
    const [firstengState, setFirstEngSelect] = useState(
    {
        loading_firsteng: false,
        id_firsteng: null,
    })
    useEffect(() => {
        setFirstEngSelect({loading_firsteng: true})
        axios.get(apiUrl_engtech).then((resp) => {
            const allFirstEng = resp.data;
            setFirstEngSelect({
                loading_firsteng: false,
                id_firsteng: allFirstEng
            });
        });
    }, [setFirstEngSelect]);

    const DataLoading_SecEng =  OnLoadingData(ID_SecEngSelect);
    const [secengState, setSecEngState] = useState(
    {
        loading_seceng: false,
        id_seceng: null,
    })
    useEffect(() => {
        setSecEngState({loading_seceng: true})
        axios.get(apiUrl_engtech).then((resp) => {
            const allSecEng = resp.data;
            setSecEngState({
                loading_seceng: false,
                id_seceng: allSecEng
            });
        });
    }, [setSecEngState]);

    const DataLoading_MainEng =  OnLoadingData(ID_MainEngSelect);
    const [mainengState, setMainEngState] = useState(
    {
        loading_maineng: false,
        id_maineng: null,
    })
    useEffect(() => {
        setMainEngState({loading_maineng: true})
        axios.get(apiUrl_engtech).then((resp) => {
            const allMainEng = resp.data;
            setMainEngState({
                loading_maineng: false,
                id_maineng: allMainEng
            });
        });
    }, [setMainEngState]);

    const DataLoading_FirstTech =  OnLoadingData(ID_FirstTechSelect);
    const [firsttechState, setFirstTechSelect] = useState(
    {
        loading_firsttech: false,
        id_firsttech: null,
    })
    useEffect(() => {
        setFirstTechSelect({loading_firsttech: true})
        axios.get(apiUrl_engtech).then((resp) => {
            const allFirstTech = resp.data;
            setFirstTechSelect({
                loading_firsttech: false,
                id_firsttech: allFirstTech
            });
        });
    }, [setFirstTechSelect]);

    const DataLoading_SecTech =  OnLoadingData(ID_SecTechSelect);
    const [sectechState, setSecTechState] = useState(
    {
        loading_sectech: false,
        id_sectech: null,
    })
    useEffect(() => {
        setSecTechState({loading_sectech: true})
        axios.get(apiUrl_engtech).then((resp) => {
            const allSecTech = resp.data;
            setSecTechState({
                loading_seceng: false,
                id_sectech: allSecTech
            });
        });
    }, [setSecTechState]);

    const[modal,SetModal]=useState({
        modal1: false,
        modal2: false,
        modal3: false
    })

    const modal_edit =(
        <form method="POST" action="">
            <div className="formas_modal">
                <p> 
                    <span id="text_label">ID</span> 
                    <select id="selectID" name="Id_fp" placeholder="ID"> 
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                    </select> 
                    <button name="search" class="searchs"> Найти </button>
                </p>  
                <p class="com1"></p>
                <p> 
                    <span id="text_label">Номер акта</span> 
                    <input type="text" name="surname" placeholder="Номер акта"/> 
                </p>  
                <p class="com1"></p>
                <p> 
                    <span id="text_label">Дата составления акта</span> 
                    <input type="text" name="name" placeholder="Дата составления акта"/> 
                </p>  
                <p class="com1"></p>
                <p> 
                    <span id="text_label">Воздушное судно</span> 
                    <DataLoading_Aircraft isLoading={aircraftState.loading_aircraft} id_aircraft={aircraftState.id_aircraft} /> 
                </p>  
                <p class="com1"></p>
                <p> 
                    <span id="text_label">ФИО Главного инженера</span>
                    <DataLoading_MainEng isLoading={mainengState.loading_maineng} id_maineng={mainengState.id_maineng}/>
                </p>  
                <p class="com1"></p> 
                <p> 
                    <span id="text_label">ФИО Первого инженера</span> 
                    <DataLoading_FirstEng isLoading={firstengState.loading_firsteng} id_firsteng={firstengState.id_firsteng}/>
                </p>  
                <p class="com1"></p>
                <p> 
                    <span id="text_label">ФИО Второго инженера</span>
                    <DataLoading_SecEng isLoading={secengState.loading_seceng} id_seceng={secengState.id_seceng}/>
                </p>  
                <p class="com1"></p>
                <p>
                    <span id="text_label">ФИО Первого техника</span>
                    <DataLoading_FirstTech isLoading={firsttechState.loading_firsttech} id_firsttech={firsttechState.id_firsttech}/>
                </p>  
                <p class="com1"></p>
                <p>
                    <span id="text_label">ФИО Второго техника</span>
                    <DataLoading_SecTech isLoading={sectechState.loading_sectech} id_sectech={sectechState.id_sectech}/>
                </p>  
                <p class="com1"></p>
                <p>
                    <span id="text_label">Дата технического обслуживания</span>
                    <input type="text" name="diploma" placeholder="Дата технического обслуживания"/>
                </p>  
                <p class="com1"></p>
                <p>
                    <span id="text_label"> Причина технического обслуживания </span>
                    <input type="text" name="date_empl" placeholder="Причина технического обслуживания"/>
                </p>  
                <p class="com1"></p>
                <p>
                    <span id="text_label"> Результат </span>
                    <input type="text" name="date_empl" placeholder="Результат"/>
                </p>  
                <p class="com1"></p>
                <input type="submit" value="СОХРАНИТЬ" name="save"/>                
            </div>            
        </form>
    );

    const modal_add  = (
        <form method="POST" action="">
            <div className="formas_modal">
                <p>
                    <span id="text_label">Номер акта</span> 
                    <input type="text" name="surname" placeholder="Номер акта"/>
                </p>  
                <p class="com1"></p>
                <p>
                    <span id="text_label">Дата составления акта</span>
                    <input type="text" name="name" placeholder="Дата составления акта"/>
                </p>  
                <p class="com1"></p>
                <p>
                    <span id="text_label">Воздушное судно</span>
                    <DataLoading_Aircraft isLoading={aircraftState.loading} id_aircraft={aircraftState.id_aircraft} />
                </p>  
                <p class="com1"></p>
                <p> 
                    <span id="text_label">ФИО Главного инженера</span>
                    <DataLoading_MainEng isLoading={mainengState.loading_maineng} id_maineng={mainengState.id_maineng}/>
                </p>  
                <p class="com1"></p> 
                <p> 
                    <span id="text_label">ФИО Первого инженера</span> 
                    <DataLoading_FirstEng isLoading={firstengState.loading_firsteng} id_firsteng={firstengState.id_firsteng}/>
                </p>  
                <p class="com1"></p>
                <p> 
                    <span id="text_label">ФИО Второго инженера</span>
                    <DataLoading_SecEng isLoading={secengState.loading_seceng} id_seceng={secengState.id_seceng}/>
                </p>  
                <p class="com1"></p>
                <p>
                    <span id="text_label">ФИО Первого техника</span>
                    <DataLoading_FirstTech isLoading={firsttechState.loading_firsttech} id_firsttech={firsttechState.id_firsttech}/>
                </p>  
                <p class="com1"></p>
                <p>
                    <span id="text_label">ФИО Второго техника</span>
                    <DataLoading_SecTech isLoading={sectechState.loading_sectech} id_sectech={sectechState.id_sectech}/>
                </p>  
                <p class="com1"></p>
                <p>
                    <span id="text_label">Дата технического обслуживания</span>
                    <input type="text" name="diploma" placeholder="Дата технического обслуживания"/>
                </p>  
                <p class="com1"></p>
                <p> 
                    <span id="text_label"> Причина технического обслуживания </span>
                    <input type="text" name="date_empl" placeholder="Причина технического обслуживания"/>
                </p>  
                <p class="com1"></p>
                <p>
                    <span id="text_label"> Результат </span>
                    <input type="text" name="date_empl" placeholder="Результат"/>
                </p>  
                <p class="com1"></p>
                <input type="submit" value="СОХРАНИТЬ" name="save"/>
            </div>                
        </form>
    ); 

    const modal_delete = ( 
        <form method="POST" action="">
            <div class="formas_modal">
                <p>
                    <span id="text_label">ID</span>
                    <input type="text" name="id" class="del_info" placeholder="ID"/>
                </p>  
                <p class="com1"></p>
                <input type="submit" class="btn_del_info"  value="СОХРАНИТЬ" name="save"/>   
            </div>                
        </form>
    );

    return(
        <div id="buts"> 
            <button class="but_setting" id="but_add" name='add' onClick={()=> SetModal({...modal, modal1:true})}>
                Добавить 
            </button>
            <Modal title="Добавить информацию" 
                isOpened={modal.modal1} 
                onModalClose={()=>SetModal({...modal, modal1:false})}>
                {modal_add}
            </Modal>   
            <button class="but_setting" id="but_edit" name='edits_info'  onClick={()=> SetModal({...modal, modal2:true})}> 
                Изменить
            </button>
            <Modal title="Изменить информацию" 
                isOpened={modal.modal2} 
                onModalClose={()=>SetModal({...modal, modal2:false})}>
                {modal_edit}    
            </Modal>
            <button class="but_setting" id="but_del" name='del' onClick={()=> SetModal({...modal, modal3:true})}>
                Удалить
            </button>
            <Modal title="Удалить информацию" 
                isOpened={modal.modal3} 
                onModalClose={()=>SetModal({...modal, modal3:false})}>
                {modal_delete}    
            </Modal>
        </div>
    );
}

export default ButtonMA_edits;