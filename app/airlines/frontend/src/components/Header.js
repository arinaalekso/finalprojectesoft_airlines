import React  from 'react';
import {BrowserRouter as Router, Routes, Route, Link} from "react-router-dom";
import img_iconsairlines from './img/icons-airlines.png';
import img_exit from './img/exit.svg';
import {observer} from "mobx-react-lite";
import SessionStore from '../store/SessionStore';
import './css/header.css';
import { useContext } from 'react';
import { Context } from '..';
import { AIRCRAFT_ROUTE, AUTH_ROUTE, ENGINEERTECHNICAL_ROUTE, FLIGHTPERSONAL_ROUTE, FLIGHT_ROUTE, MR_ROUTE, PERSONALACC_ROUTE } from '../utilis/consts';

const Header = observer(()=>
{
    const logOut = () => {
        user.setUser({})
        user.setIsAuth(false)
    } 
    const [state, setState] = React.useState({ activeItem: 1 });
    const handleClick = (event, { index }) => setState({ activeItem: index });
    const {user} = useContext(Context)
    console.log(user)
    if (user.isAuth)
    {
        return(
            <header>
                <div id="logo" onClick={(event) => handleClick(event, { index: 0})}>
                    <Link to={PERSONALACC_ROUTE}>
                        <img src={img_iconsairlines}/>
                    </Link>
                </div>
                <div id="nav-panel">
                    <ul className='menu'>
                        <Link to={FLIGHT_ROUTE}>
                            <li className={state.activeItem === 1 ? 'li_active' : ''} onClick={(event) => handleClick(event, { index: 1 })}>
                                <div id={state.activeItem === 1 ? 'li_active' : ''}> 
                                    Рейс
                                </div>
                            </li>
                        </Link>
                        <Link to={FLIGHTPERSONAL_ROUTE}>
                            <li className={state.activeItem === 2 ? 'li_active' : ''} onClick={(event) => handleClick(event, { index: 2 })}>
                                <div id={state.activeItem === 2 ? 'li_active' : ''}> 
                                    Летная служба
                                </div>
                            </li>
                        </Link>
                        <Link to={ENGINEERTECHNICAL_ROUTE}>
                            <li className={state.activeItem === 3 ? 'li_active' : ''} onClick={(event) => handleClick(event, { index: 3 })}>
                                <div id={state.activeItem === 3 ? 'li_active' : ''}> 
                                    Инженерно-технический отдел
                                </div>
                            </li>
                        </Link>
                        <Link to={AIRCRAFT_ROUTE}>
                            <li className={state.activeItem === 4 ? 'li_active' : ''} onClick={(event) => handleClick(event, { index: 4 })}>
                                <div id={state.activeItem === 4 ? 'li_active' : ''}> 
                                    Воздушное судно
                                </div>
                            </li>
                        </Link>
                        <Link to={MR_ROUTE}>
                            <li className={state.activeItem === 5 ? 'li_active' : ''} onClick={(event) => handleClick(event, { index: 5 })}>
                                <div id={state.activeItem === 5 ? 'li_active' : ''}> 
                                    Акт Технического Обслуживания
                                </div>
                            </li>
                        </Link>
                    </ul>
                </div>
                <Link to={AUTH_ROUTE}>    
                    <button onClick={() => logOut()}>
                            <img src={img_exit}/>        
                    </button>
                </Link>
            </header>
        );
    }
});

export default Header;