import React, { useState, useEffect } from 'react';
import Modal from './modal_windows';
import '../components/css/add_edit_info.css'; 
import AircraftSelect from "./selectes/AircraftSelect";
import ID_EngSelect from "./selectes/ID_EngSelect";
import ID_FirstFlightPersSelect from './selectes/ID_FirstFlightPersSelect';
import ID_SecFlightPersSelect from './selectes/ID_SecFlightPersSelect';
import OnLoadingData from "./LoadingDataModal";
import FlightSelect from './selectes/FlightSelect';
import axios from "axios";

const apiUrl_aircraft = 'http://localhost:3001/aircraft';
const apiUrl_engtech = 'http://localhost:3001/eng_tech';
const apiUrl_fightper = 'http://localhost:3001/flightpersonal';
const apiUrl_flight = 'http://localhost:3001/flight';


function Buttonflight_edits()
{
    const DataLoading_Flight =  OnLoadingData(FlightSelect);
    const [flightState, setFlightState] = useState(
    {
        loading_flight: false,
        id_flight: null,
    })
    useEffect(() => {
        setFlightState({loading_flight: true})
        axios.get(apiUrl_flight).then((resp) => {
            const allFlight = resp.data;
            setFlightState({
                loading_flight: false,
                id_flight: allFlight
            });
        });
    }, [setFlightState]);

    const DataLoading_Aircraft =  OnLoadingData(AircraftSelect);
    const [aircraftState, setAircraftState] = useState(
    {
        loading_aircraft: false,
        id_aircraft: null,
    })
    useEffect(() => {
        setAircraftState({loading_aircraft: true})
        axios.get(apiUrl_aircraft).then((resp) => {
            const allAircraft = resp.data;
            setAircraftState({
                loading_aircraft: false,
                id_aircraft: allAircraft
            });
        });
    }, [setAircraftState]);

    const DataLoading_Eng =  OnLoadingData(ID_EngSelect);
    const [engState, setEngState] = useState(
    {
        loading_eng: false,
        id_eng: null,
    })
    useEffect(() => {
        setEngState({loading_eng: true})
        axios.get(apiUrl_engtech).then((resp) => {
            const allEng = resp.data;
            setEngState({
                loading_eng: false,
                id_eng: allEng
            });
        });
    }, [setEngState]);

    const DataLoading_FFP =  OnLoadingData(ID_FirstFlightPersSelect);
    const [ffpState, setFFPState] = useState(
    {
        loading_ffp: false,
        id_firstflightpers: null,
    })
    useEffect(() => {
        setFFPState({loading_ffp: true})
        axios.get(apiUrl_fightper).then((resp) => {
            const allFFP = resp.data;
            setFFPState({
                loading_ffp: false,
                id_firstflightpers: allFFP
            });
        });
    }, [setFFPState]);

    const DataLoading_SFP =  OnLoadingData(ID_SecFlightPersSelect);
    const [sfpState, setSFPState] = useState(
    {
        loading_sfp: false,
        id_secflightpers: null,
    })
    useEffect(() => {
        setSFPState({loading_sfp: true})
        axios.get(apiUrl_fightper).then((resp) => {
            const allSFP = resp.data;
            setEngState({
                loading_sfp: false,
                id_secflightpers: allSFP
            });
        });
    }, [setSFPState]);

    const[modal,SetModal]=useState({
        modal1: false,
        modal2: false,
        modal3: false
    })

    const modal_add=(
        <form method="POST" action="">
            <div className="formas_modal">
                <p> 
                    <span id="text_label">Воздушное судно</span> 
                    <DataLoading_Aircraft isLoading={aircraftState.loading_aircraft} id_aircraft={aircraftState.id_aircraft} /> 
                </p>
                <p> 
                    <span id="text_label">ФИО Первого летчика</span>
                    <DataLoading_FFP isLoading={ffpState.loading_ffp} id_firstflightpers={ffpState.id_firstflightpers} />
                </p>  
                <p class="com1"></p>
                <p> 
                    <span id="text_label">ФИО Второго летчика</span> 
                    <DataLoading_SFP isLoading={sfpState.loading_sfp} id_secflightpers={sfpState.id_secflightpers}  />
                </p>  
                <p class="com1"></p>
                <p> 
                    <span id="text_label">ФИО Инженера</span> 
                    <DataLoading_Eng isLoading={engState.loading_eng} id_eng={engState.id_eng} />
                </p>  
                <p class="com1"></p> 
                <p>
                    <span id="text_label">Дата рейса</span>
                    <input type="text" name="date_flight" placeholder="Дата рейса"/>
                </p>  
                <p class="com1"></p>
                <p>
                    <span id="text_label">Вид груза</span>
                    <input type="text" name="cargo_type" placeholder="Вид груза"/>
                </p>  
                <p class="com1"></p>
                <p>
                    <span id="text_label">Маршрут рейса</span>
                    <input type="text" name="flight_route" placeholder="Маршрут рейса"/>
                </p>  
                <p class="com1"></p> 
                <p>
                    <span id="text_label">Летное время в рейсе</span>
                    <input type="text" name="time_on_flight" placeholder="Летное время в рейсе"/>
                </p>                    
                <p class="com1"></p>
                <input type="submit" value="СОХРАНИТЬ" name="save"/>
            </div>            
        </form>
    );

    const modal_edit = (
        <form method="POST">
            <div className="formas_modal">
                <p> 
                    <span id="text_label">ID</span> 
                    <DataLoading_Flight isLoading={flightState.loading_flight} id_flight={flightState.id_flight}/> 
                    <button name="search" class="searchs"> Найти </button>
                </p>  
                <p> 
                    <span id="text_label">Воздушное судно</span> 
                    <DataLoading_Aircraft isLoading={aircraftState.loading_aircraft} id_aircraft={aircraftState.id_aircraft} /> 
                </p>
                <p> 
                    <span id="text_label">ФИО Первого летчика</span>
                    <DataLoading_FFP isLoading={ffpState.loading_ffp} id_firstflightpers={ffpState.id_firstflightpers} />
                </p>  
                <p class="com1"></p>
                <p> 
                    <span id="text_label">ФИО Второго летчика</span> 
                    <DataLoading_SFP isLoading={sfpState.loading_sfp} id_secflightpers={sfpState.id_secflightpers}  />
                </p>  
                <p class="com1"></p>
                <p> 
                    <span id="text_label">ФИО Инженера</span> 
                    <DataLoading_Eng isLoading={engState.loading_eng} id_eng={engState.id_eng} />
                </p>  
            <p class="com1"></p> 
            <p>
                <span id="text_label">Дата рейса</span>
                <input type="text" name="date_flight" placeholder="Дата рейса"/>
            </p>  
            <p class="com1"></p>
            <p>
                <span id="text_label">Вид груза</span>
                <input type="text" name="cargo_type" placeholder="Вид груза"/>
            </p>  
            <p class="com1"></p>
            <p>
                <span id="text_label">Маршрут рейса</span>
                <input type="text" name="flight_route" placeholder="Маршрут рейса"/>
            </p>  
            <p class="com1"></p> 
            <p>
                <span id="text_label">Летное время в рейсе</span>
                <input type="text" name="time_on_flight" placeholder="Летное время в рейсе"/>
            </p>                    
            <p class="com1"></p>
            <input type="submit" value="СОХРАНИТЬ" name="save"/>   
            </div>              
        </form>     
    );

    const modal_delete = ( 
        <form method="POST" action="">
            <div class="formas_modal">
                <p>
                    <span id="text_label">ID</span>
                    <input type="text" name="id" class="del_info" placeholder="ID"/>
                </p>  
                <p class="com1"></p>
                <input type="submit" class="btn_del_info"  value="СОХРАНИТЬ" name="save"/>   
            </div>                
        </form>
    ); 

    return(
        <div id="buts"> 
            <button class="but_setting" id="but_add" name='add' onClick={()=> SetModal({...modal, modal1:true})}>
                Добавить 
            </button>
            <Modal title="Добавить информацию" 
                isOpened={modal.modal1} 
                onModalClose={()=>SetModal({...modal, modal1:false})}>
                {modal_add}
            </Modal>   
            <button class="but_setting" id="but_edit" name='edits_info'  onClick={()=> SetModal({...modal, modal2:true})}> 
                Изменить
            </button>
            <Modal title="Изменить информацию" 
                isOpened={modal.modal2} 
                onModalClose={()=>SetModal({...modal, modal2:false})}>
                {modal_edit}    
            </Modal>
            <button class="but_setting" id="but_del" name='del' onClick={()=> SetModal({...modal, modal3:true})}>
                Удалить
            </button>
            <Modal title="Удалить информацию" 
                isOpened={modal.modal3} 
                onModalClose={()=>SetModal({...modal, modal3:false})}>
                {modal_delete}    
            </Modal>
        </div>
    );
}

export default Buttonflight_edits;