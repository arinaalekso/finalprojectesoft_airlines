function OnLoadingData(Component) {
    return function LoadingDatas({ isLoading, ...props }) {
        if (!isLoading) return <Component {...props} />

        else return (
            <div>
                <h3>Подождите, данные загружаются!</h3>
            </div>
        )
    }
}

export default OnLoadingData;