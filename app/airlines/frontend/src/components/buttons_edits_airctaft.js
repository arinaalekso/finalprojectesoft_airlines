import React, { useState,useEffect } from 'react';
import Modal from './modal_windows';
import '../components/css/add_edit_info.css';  
import AircraftSelect from "./selectes/AircraftSelect";
import OnLoadingData from "./LoadingDataModal";
import axios from "axios";

const apiUrl_aircraft = 'http://localhost:3001/aircraft';

function ButtonAircraft_edits()
{
    const DataLoading =  OnLoadingData(AircraftSelect);
    const [aircraftState, setAircraftState] = useState(
    {
        loading: false,
        id_aircraft: null,
    })
    useEffect(() => {
        setAircraftState({loading: true})
        axios.get(apiUrl_aircraft).then((resp) => {
            const allAircraft = resp.data;
            setAircraftState({
                loading: false,
                id_aircraft: allAircraft
            });
        });
    }, [setAircraftState]);

    const[modal,SetModal]=useState({
        modal1: false,
        modal2: false,
        modal3: false
    })

    const modal_add=(
        <form method="POST" action="">
            <div className="formas_modal">
                <p>
                    <span id="text_label">Вид</span>
                    <input type="text" name="vid" placeholder="Вид"/>
                </p>  
                <p className="com1"></p>
                <p>
                    <span id="text_label">Тип</span>
                    <input type="text" name="type" placeholder="Тип"/>
                </p>  
                <p className="com1"></p>
                <p>
                    <span id="text_label">Класс</span> <input type="text" name="class" placeholder="Класс"/>
                </p>  
                <p className="com1"></p>
                <p>
                    <span id="text_label">Объем перевозок</span>
                    <input type="text" name="volume_of_traffic" placeholder="Объем перевозок"/>
                </p>  
                <p className="com1"></p> 
                <p>
                    <span id="text_label">Год выпуска</span>
                    <input type="text" name="year_of_release" placeholder="Год выпуска"/>
                </p>  
                <p className="com1"></p>
                <p>
                    <span id="text_label">Дата крайнего технического обслуживания</span>
                    <input type="text" name="date_lasttech" placeholder="Дата крайнего технического обслуживания"/>
                </p>  
                <p className="com1"></p>
                <p>
                    <span id="text_label"> Назначенный ресурс </span>
                    <input type="text" name="specified_life" placeholder="Назначенный ресурс"/>
                </p>                    
                <input type="submit" value="СОХРАНИТЬ" name="save"/>   
            </div>            
        </form>
    );

    const modal_edit = (
        <form method="POST" action="">
            <div class="formas_modal">
                <p> 
                    <span id="text_label">ID</span>
                    <DataLoading isLoading={aircraftState.loading} id_aircraft={aircraftState.id_aircraft} /> 
                    <button name="search" class="searchs"> Найти </button>
                </p>  
                <p class="com1"></p>
                <p> <span id="text_label">Вид</span> <input type="text" name="vid" placeholder="Вид"/> </p>  
                <p class="com1"></p>
                <p> <span id="text_label">Тип</span> <input type="text" name="type" placeholder="Тип"/> </p>  
                    <p class="com1"></p>
                    <p> <span id="text_label">Класс</span> <input type="text" name="class" placeholder="Класс"/> </p>  
                    <p class="com1"></p>
                    <p> <span id="text_label">Объем перевозок</span> <input type="text" name="volume_of_traffic" placeholder="Объем перевозок"/> </p>  
                    <p class="com1"></p> 
                    <p> <span id="text_label">Год выпуска</span> <input type="text" name="year_of_release" placeholder="Год выпуска"/> </p>  
                    <p class="com1"></p>
                    <p> <span id="text_label">Дата крайнего технического обслуживания</span><input type="text" name="date_lasttech" placeholder="Дата крайнего технического обслуживания"/> </p>  
                    <p class="com1"></p>
                    <p> <span id="text_label"> Назначенный ресурс </span><input type="text" name="specified_life" placeholder="Назначенный ресурс"/> </p>                    
                    <input type="submit" value="СОХРАНИТЬ" name="save"/>   
                </div>                
            </form>     
    );

    const modal_delete = ( 
        <form method="POST" action="">
            <div class="formas_modal">
                <p>
                    <span id="text_label">ID</span>
                    <input type="text" name="id" class="del_info" placeholder="ID"/>
                </p>  
                <p class="com1"></p>
                <input type="submit" class="btn_del_info"  value="СОХРАНИТЬ" name="save"/>   
            </div>                
        </form>
    ); 

    return(
        <div id="buts"> 
            <button class="but_setting" id="but_add" name='add' onClick={()=> SetModal({...modal, modal1:true})}>
                Добавить 
            </button>
            <Modal title="Добавить информацию" 
                isOpened={modal.modal1} 
                onModalClose={()=>SetModal({...modal, modal1:false})}>
                {modal_add}
            </Modal>   
            <button class="but_setting" id="but_edit" name='edits_info'  onClick={()=> SetModal({...modal, modal2:true})}> 
                Изменить
            </button>
            <Modal title="Изменить информацию" 
                isOpened={modal.modal2} 
                onModalClose={()=>SetModal({...modal, modal2:false})}>
                {modal_edit}    
            </Modal>
            <button class="but_setting" id="but_del" name='del' onClick={()=> SetModal({...modal, modal3:true})}>
                Удалить
            </button>
            <Modal title="Удалить информацию" 
                isOpened={modal.modal3} 
                onModalClose={()=>SetModal({...modal, modal3:false})}>
                {modal_delete}    
            </Modal>
        </div>
    );
}

export default ButtonAircraft_edits;