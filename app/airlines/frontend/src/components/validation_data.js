function DataValidation() 
{
    if (login.value !== "" && password.value !== "") 
    {
        submitInput.style.backgroundColor = "#60C2AA";
    }
    else 
    {
        submitInput.style.backgroundColor = "";
    }
}

const CheckingInputLogin = () => {
    login = document.getElementsByName('login')[0]
    login.value = login.value.replace(/[^\da-z._A-Z]/g, "")
    };
    
const CheckingInputLoginPassword = () => {
    password = document.getElementsByName('password')[0] 
    password.value = password.value.replace(/[^\da-z._A-Z]/g, "")
    };

document.querySelector('input[name=login]').oninput = CheckingInputLogin;
document.querySelector('input[name=password]').oninput = CheckingInputLoginPassword;
login.addEventListener("input", DataValidation);
password.addEventListener("input", DataValidation);
