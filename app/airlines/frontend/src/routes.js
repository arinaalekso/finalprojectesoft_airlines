import Aircraft from "./pages/Aircraft";
import Auth from "./pages/Auth";
import Engineer_Technical from "./pages/Engineer_technical";
import Flight from "./pages/Flight";
import Flightpersonal from "./pages/Flightpersonal";
import MR from "./pages/MR";
import PersonalAcc from "./pages/PersonalAcc";
import { AIRCRAFT_ROUTE, ENGINEERTECHNICAL_ROUTE, FLIGHTPERSONAL_ROUTE, FLIGHT_ROUTE, MR_ROUTE, PERSONALACC_ROUTE, AUTH_ROUTE } from "./utilis/consts"

export const authRoutes=[
    {
        path: FLIGHT_ROUTE,
        Component: Flight
    },

    {
        path: FLIGHTPERSONAL_ROUTE,
        Component: Flightpersonal
    },
    {
        path: ENGINEERTECHNICAL_ROUTE,
        Component: Engineer_Technical
    },
    {
        path: AIRCRAFT_ROUTE,
        Component: Aircraft
    },
    {
        path: MR_ROUTE,
        Component: MR
    },
    {
        path:PERSONALACC_ROUTE,
        Component: PersonalAcc
    }
]

export const publicRoutes=[
    {
        path: AUTH_ROUTE,
        Component: Auth
    }
]