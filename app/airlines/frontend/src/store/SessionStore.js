import { makeObservable, observable, action } from 'mobx';

class SessionArch 
{
    isAuthoLogin = false;
    ID_session = null;
    constructor(){
        makeObservable(this, {
            isAuthoLogin: observable, 
            ID_session: observable, 
            processEntry : action,
        });
    }
    processEntry() 
    {
        SessionStore.isAuthoLogin = true;
        SessionStore.ID_session = 1;
        console.info(SessionStore.isAuthoLogin);
        localStorage.setItem('isAuthoLogin', SessionStore.isAuthoLogin);
        localStorage.setItem('ID_session', SessionStore.ID_session);
        console.info(localStorage.getItem('ID_session'));
    }
    processExit() 
    {
        SessionStore.isAuthoLogin = false;
        SessionStore.ID_session = null;
        console.info(SessionStore.isAuthoLogin);
        localStorage.setItem('isAuthoLogin', SessionStore.isAuthoLogin);
        localStorage.setItem('ID_session', SessionStore.ID_session);
        document.location.reload();
    }
}

const SessionStore = new SessionArch();
export default SessionStore;