import React,{useEffect,useState} from "react";
import {observer} from "mobx-react-lite";
import '../components/css/forma.css';
import ButtonMA_edits from "../components/buttons_edits_maintenancereport";
import axios from "axios";
import MRData from "../components/tables/MRTable";
import OnLoadingData from "../components/LoadingData";

const apiUrl = 'http://localhost:3001/mr';
const MR=()=>
{
    const DataLoading =  OnLoadingData(MRData);
    const [mrState, setMRState] = useState(
    {
        loading: false,
        mr: null,
    })
    useEffect(() => {
        setMRState({loading: true})
        axios.get(apiUrl).then((resp) => {
            const allMR = resp.data;
            setMRState({
                loading: false,
                mr: allMR
            });
        });
    }, [setMRState]);
    return(
        <div className="forma">
            <h1 id="h1_rating">Акт Технического Обслуживания</h1>
            <DataLoading isLoading={mrState.loading} mr={mrState.mr} />
            <ButtonMA_edits/>
        </div>
    );
};

export default MR