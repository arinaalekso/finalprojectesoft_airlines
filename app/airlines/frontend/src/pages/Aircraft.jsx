import React,{useEffect,useState} from "react";
import { Component } from "react";
import {observer} from "mobx-react-lite";
import '../components/css/forma.css';
import ButtonAircraft_edits from "../components/buttons_edits_airctaft";
import axios from "axios";
import AircraftData from "../components/tables/AircraftTable";
import OnLoadingData from "../components/LoadingData";

const apiUrl = 'http://localhost:3001/aircraft';
const Aircraft=()=>
{
    const DataLoading =  OnLoadingData(AircraftData);
    const [aircraftState, setAircraftState] = useState(
    {
        loading: false,
        aircraft: null,
    })
    useEffect(() => {
        setAircraftState({loading: true})
        axios.get(apiUrl).then((resp) => {
            const allAircraft = resp.data;
            setAircraftState({
                loading: false,
                aircraft: allAircraft
            });
        });
    }, [setAircraftState]);
    return(
        <div className="forma">
            <h1 id="h1_rating">Воздушное судно</h1>    
            <DataLoading isLoading={aircraftState.loading} aircraft={aircraftState.aircraft} />
            <ButtonAircraft_edits/>
        </div>
    );
};

export default Aircraft