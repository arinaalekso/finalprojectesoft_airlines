import React,{useEffect,useState} from "react";
import {observer} from "mobx-react-lite";
import '../components/css/personalacc.css'
import axios from "axios";

const PersonalAcc= ()=>
{
    return(
        <div className="body-reg">
            <div className="forma-reg">
                <h1 className="h1_reg">Личный кабинет</h1>
                <form method="POST" action="">
                    <div className="reg_forma">
                        <p> <span id="text_label">Фамилия</span> <input type="text" name="surname" placeholder="Фамилия"/> </p>  
                        <p className="com1"></p>
                        <p> <span id="text_label"> Имя</span> <input type="text" name="name" placeholder="Имя"/> </p>  
                        <p className="com1"></p>
                        <p> <span id="text_label">Отчество</span> <input type="text" name="patronymic" placeholder="Отчество"/> </p>  
                        <p className="com1"></p>
                        <p> <span id="text_label">Дата рождения</span> <input type="text" name="dr" placeholder="Дата рождения"/> </p>  
                        <p className="com1"></p> 
                        <p> <span id="text_label">Номер телефона</span> <input type="text" name="phonenumber" placeholder="Номер телефона"/> </p>  
                        <p className="com1"></p>
                        <p> <span id="text_label"> Эл. Почта </span><input type="text" name="email" placeholder="Эл. Почта"/> </p>  
                        <p className="com1"></p>
                        <p> <span id="text_label"> Должность </span><input type="text" name="post" placeholder="Должность"/> </p>  
                        <p className="com1"></p>
                        <p> <span id="text_label"> Отдел </span>  <input type="text" name="department" placeholder="Отдел"/> </p>  
                        <p className="com1"></p>
                        <p> <span id="text_label"> Диплом </span>  <input type="text" name="diploma" placeholder="Диплом"/> </p>  
                        <p className="com1"></p>
                        <p> <span id="text_label"> Дата устройства на работу  </span><input type="text" name="date_empl" placeholder="Дата устройства на работу"/> </p>  
                        <p className="com1"></p>
                        <p> <span id="text_label"> Логин </span> <input type="text" name="login" placeholder="Логин"/> </p>  
                        <p className="com1"></p>
                        <p><span id="text_label"> Пароль </span>  <input type="text" name="password" placeholder="Пароль"/></p>
                        <p className="com2"></p>
                        <input type="submit" value="ИЗМЕНИТЬ" name="reges"/>   
                    </div>                
                </form>
            </div>
        </div>
    );
};

export default PersonalAcc