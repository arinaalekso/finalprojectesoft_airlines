import React,{useEffect,useState} from "react";
import {observer} from "mobx-react-lite";
import '../components/css/forma.css';
import axios from "axios";
import ButtonET_edits from "../components/buttons_edits_engineer_technical";
import EngTechData from "../components/tables/EngTechTable";
import OnLoadingData from "../components/LoadingData";

const apiUrl = 'http://localhost:3001/eng_tech';
const Engineer_Technical=()=>
{
    const DataLoading =  OnLoadingData(EngTechData);
    const [eng_techState, setEngTechState] = useState(
    {
        loading: false,
        engtech: null,
    })
    useEffect(() => {
        setEngTechState({loading: true})
        axios.get(apiUrl).then((resp) => {
            const allEngTech = resp.data;
            setEngTechState({
                loading: false,
                engtech: allEngTech
            });
        });
    }, [setEngTechState]);
    return(
        <div className="forma">
            <h1 id="h1_rating">Инженерно-технический отдел</h1>
                <DataLoading isLoading={eng_techState.loading} engtech={eng_techState.engtech} />
            <ButtonET_edits/>
        </div>
    );
};

export default Engineer_Technical