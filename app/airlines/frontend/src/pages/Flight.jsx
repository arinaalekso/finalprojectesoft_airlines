import React,{useEffect,useState} from "react";
import {observer} from "mobx-react-lite";
import '../components/css/forma.css';
import Buttonflight_edits from "../components/buttons_edits_flight";
import FlightData from "../components/tables/FlightTable";
import OnLoadingData from "../components/LoadingData";
import axios from "axios";

const apiUrl = 'http://localhost:3001/flight';
const Flight=()=>
{
    const DataLoading =  OnLoadingData(FlightData);
    const [flightState, setFlightState] = useState(
    {
        loading: false,
        flight: null,
    })
    useEffect(() => {
        setFlightState({loading: true})
        axios.get(apiUrl).then((resp) => {
            const allFlight = resp.data;
            setFlightState({
                loading: false,
                flight: allFlight
            });
        });
    }, [setFlightState]);
    return(
    <div className="forma">
        <h1 id="h1_rating">Рейс</h1>
            <DataLoading isLoading={flightState.loading} flight={flightState.flight} />
        <Buttonflight_edits/>
    </div>
    );
};

export default Flight