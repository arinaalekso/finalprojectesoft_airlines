import React,{useEffect,useState} from "react";
import {observer} from "mobx-react-lite";
import '../components/css/forma.css';
import ButtonFlightPersonal_edits from "../components/buttons_edits_flightpersonal";
import axios from "axios";
import FlightPersData from "../components/tables/FlightPersTable";
import OnLoadingData from "../components/LoadingData";

const apiUrl = 'http://localhost:3001/flightpersonal';
const Flightpersonal= ()=>
{
    const DataLoading =  OnLoadingData(FlightPersData);
    const [flightpersonalState, setFlightPersState] = useState(
    {
        loading: false,
        flightpersonal: null,
    })
    useEffect(() => {
        setFlightPersState({loading: true})
        axios.get(apiUrl).then((resp) => {
            const allFlightPers = resp.data;
            setFlightPersState({
                loading: false,
                flightpersonal: allFlightPers
            });
        });
    }, [setFlightPersState]);
    return(
        <div className="forma">
            <h1 id="h1_rating">Летная служба</h1>
                <DataLoading isLoading={flightpersonalState.loading} flightpersonal={flightpersonalState.flightpersonal} />
            <ButtonFlightPersonal_edits/>
        </div>
    );
};

export default Flightpersonal