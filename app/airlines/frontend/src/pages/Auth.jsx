import React, { useContext, useState }from "react";
import ButtonReg from '../components/buttons_reg';
import SessionStore from "../store/SessionStore";
import {BrowserRouter as Router, Routes, Route, Link} from "react-router-dom";
import { AUTH_ROUTE, FLIGHT_ROUTE } from "../utilis/consts";
import {observer} from "mobx-react-lite";
import { Context } from '..';
import { LoginCheck,registration } from "../http/userAPI"; 
import {useLocation, useHistory } from "react-router-dom/cjs/react-router-dom";

const Auth=observer(()=>
{
/*     const {user} = useContext(Context);
    const location = useLocation();
    const history = useHistory();
    const isLogin = location.pathname === AUTH_ROUTE;*/
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    /* const signIn = async () => {
        try 
        {
            let data;
            if (isLogin) {
                data = await LoginCheck(login, password);
            } else {
                data = await registration(login, password);
            }
            user.setUser(user)
            user.setIsAuth(true)
            history.push(FLIGHT_ROUTE)
        } 
        catch (e) 
        {
            alert(e.response.data.message)
        }
    } 

    console.log(user) */
    return (
        <div className="body_autho">    
            <div className="open_autho"> 
                <h1>Войдите в АИС «АВИАПРЕДПРИЯТИЕ»</h1>  
                <form>
                    <div className="open_forma">
                        <p>
                            <input type="text" name="login" required placeholder="Логин" value={login} onChange={e=> setLogin(e.target.value)}/>
                        </p>  
                        <p className="com1"></p>
                        <p>
                            <input type="password" name="password" required placeholder="Пароль" value={password} onChange={e=> setPassword(e.target.value)}/>
                        </p>
                        <p className="com2"></p>
                            <button className="open_sub" /* onClick={signIn} */>
                                ВХОД   
                            </button>
                        
                        {/*{console.log(user.isAuth)}'SessionStore.processEntry' <script src="/src/components/validation_data.js"></script> */}
                        <ButtonReg/>
                    </div>                
                </form>
            </div>
        </div>    
        );
});

export default Auth